create database ApartmentExpensesApplication;
use ApartmentExpensesApplication;

create table MonthlyMaintenance(ID int primary key identity,ResidenceID int unique,Month varchar(20),
Year int,MaintenanceAmount decimal(10,2),PaymentDate date,PaymentStatus varchar(20),
PaymentMethodID int,CategoryID int,Notes varchar(100),foreign key(PaymentMethodID) references PaymentMethods(PaymentMethodID),
foreign key(CategoryID) references ExpenseCategories(ExpenseCategoryID))

create table ApartmentResidents(ResidentID int primary key identity,
FirstName varchar(100),LastName varchar(100),FlatNumber int,OwnerShipType varchar(3),
ContactNumber varchar(10),Email varchar(50),MoveInDate date)

create table PaymentMethods(PaymentMethodID int primary key identity,PaymentMethodName varchar(50),
Description varchar(100),IsActive bit)

create table ExpenseTracking(ID int primary key identity,ExpenseDate date,
ExpenseType varchar(50),ExpenseAmount decimal(10,2),Description varchar(100),
ResidenceID int)


create table Users(UserID int primary key identity, UserName varchar(50),password varchar(100),
Email varchar(100),FullName varchar(100),CreatedAt datetime,UpdatedAt datetime)

create table Tags(TagID int primary key identity,TagName varchar(50),
Description varchar(100))

create table ExpenseTags(ExpenseTagID int primary key identity,ExpenseID int,TagID int)

create table ExpenseCategories(ExpenseCategoryID int primary key identity,CategoryID int)


create procedure RegisterUser
@UserName varchar(50),
@password varchar(100),
@Email varchar(100),
@FullName varchar(100)
as
begin
insert into Users values(@UserName,@password,@Email,@FullName,GETDATE(),GETDATE());
end

create procedure VerifyUser
@EmailID varchar(50),
@Password varchar(100)
as
begin
select * from Users where Email=@EmailID and password=@Password
end

create procedure AddExpenses
@ExpenseDate date,
@ExpenseType varchar(50),
@ExpenseAmount decimal(10,2),
@Description varchar(100),
@ResidenceID int
as
begin
insert into ExpenseTracking values(@ExpenseDate,@ExpenseType,@ExpenseAmount,@Description,@ResidenceID);
end

create procedure EditExpenses
@ExpenseDate date,
@ExpenseType varchar(50),
@ExpenseAmount decimal(10,2),
@Description varchar(100),
@ResidenceID int,
@id int
as
begin
update ExpenseTracking set ExpenseDate=@ExpenseDate , ExpenseType=@ExpenseType , ExpenseAmount=@ExpenseAmount , Description = @Description,
ResidenceID=@ResidenceID where ID=@id
end


create procedure DeleteExpenses
@id int
as
begin
delete from ExpenseTracking where ID = @id;
end