﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;

namespace ApartmentExpensesApplication.Models
{
    public class ExpensesCRUD
    {
        string constring = "Data Source=EPINHYDW06BF\\SQLEXPRESS;Initial Catalog=ApartmentExpensesApplication;Integrated Security=True";
        public List<ExpenseTracking> GetData()
        {
            SqlConnection con = new SqlConnection(constring);
            con.Close();
            con.Open();
            string query = "select * from ExpenseTracking";
            SqlCommand cmnd = new SqlCommand(query, con);
            SqlDataReader sqlDataReader = cmnd.ExecuteReader();
            List<ExpenseTracking> expenses = new List<ExpenseTracking>();
            while (sqlDataReader.Read())
            {
                ExpenseTracking expense = new ExpenseTracking();
                expense.ID =Convert.ToInt32( sqlDataReader["ID"]);
                expense.ExpenseDate = (DateTime)sqlDataReader["ExpenseDate"];
                expense.ExpenseType = sqlDataReader["ExpenseType"].ToString();
                expense.ExpenseAmount = (decimal)sqlDataReader["ExpenseAmount"];
                expense.Description = sqlDataReader["Description"].ToString();
                expense.ResidenceID = Convert.ToInt32( sqlDataReader["ResidenceID"]);
                expenses.Add(expense);
        
            }
            con.Close();
            return expenses;

        }

        public void AddExpenses(ExpenseTracking expense)
        {
            SqlConnection con = new SqlConnection( constring);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand("AddExpenses", con);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@ExpenseDate",expense.ExpenseDate);
            sqlCommand.Parameters.AddWithValue("@ExpenseType", expense.ExpenseType);
            sqlCommand.Parameters.AddWithValue("@ExpenseAmount",expense.ExpenseAmount);
            sqlCommand.Parameters.AddWithValue("@Description", expense.Description);
            sqlCommand.Parameters.AddWithValue("@ResidenceID", expense.ResidenceID);
            sqlCommand.ExecuteNonQuery();
            con.Close();
        }

        public void EditExpenses(ExpenseTracking expense)
        {
            SqlConnection con = new SqlConnection(constring);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand("EditExpenses", con);
            sqlCommand.CommandType= System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@ExpenseDate", expense.ExpenseDate);
            sqlCommand.Parameters.AddWithValue("@ExpenseType", expense.ExpenseType);
            sqlCommand.Parameters.AddWithValue("@ExpenseAmount", expense.ExpenseAmount);
            sqlCommand.Parameters.AddWithValue("@Description", expense.Description);
            sqlCommand.Parameters.AddWithValue("@ResidenceID", expense.ResidenceID);
            sqlCommand.Parameters.AddWithValue("@ID", expense.ID);

            sqlCommand.ExecuteNonQuery();
            con.Close();


        }

        public void DeleteData(int id)
        {
            SqlConnection con = new SqlConnection(constring);
            con.Open();
            SqlCommand cmnd = new SqlCommand("DeleteExpenses", con);
            cmnd.CommandType = CommandType.StoredProcedure;
            cmnd.Parameters.AddWithValue("@id", id);
            cmnd.ExecuteNonQuery();
            con.Close();
        }

    }
}