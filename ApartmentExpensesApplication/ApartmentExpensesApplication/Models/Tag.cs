﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApartmentExpensesApplication.Models
{
    public class Tag
    {
        public int  TagID { get; set; }
        public string TagName { get; set; }
        public string Description { get; set; }
    }
}