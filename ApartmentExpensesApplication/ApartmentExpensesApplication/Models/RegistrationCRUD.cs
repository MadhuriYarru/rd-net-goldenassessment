﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static System.Net.Mime.MediaTypeNames;

namespace ApartmentExpensesApplication.Models
{
    public class RegistrationCRUD
    {
        string constring = "Data Source=EPINHYDW06BF\\SQLEXPRESS;Initial Catalog=ApartmentExpensesApplication;Integrated Security=True";
        public void RegisterUser(User user)
        {
            SqlConnection con = new SqlConnection(constring);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand("RegisterUser", con);
            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@UserName", user.UserName);
            sqlCommand.Parameters.AddWithValue("@password", user.password);
            sqlCommand.Parameters.AddWithValue("@Email", user.Email);
            sqlCommand.Parameters.AddWithValue("@FullName", user.FullName);
            sqlCommand.ExecuteNonQuery();
            con.Close();


        }

        public Boolean VerifyUser(Login login)
        {
            SqlConnection con = new SqlConnection(constring);
            con.Open();
            SqlCommand sqlCommand = new SqlCommand("VerifyUser", con);
            sqlCommand.CommandType=System.Data.CommandType.StoredProcedure;
            sqlCommand.Parameters.AddWithValue("@EmailID", login.EmailID);
            sqlCommand.Parameters.AddWithValue("@Password", login.password);
            var reader = sqlCommand.ExecuteReader();
            while (reader.Read())
            {
                return true;
            }
            return false;
            
        }
    }
}