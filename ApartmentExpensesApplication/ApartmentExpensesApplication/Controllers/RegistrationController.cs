﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ApartmentExpensesApplication.Models;

namespace ApartmentExpensesApplication.Controllers
{
    public class RegistrationController : Controller
    {
        RegistrationCRUD registrationCRUD = new RegistrationCRUD(); 
        // GET: Registration
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void SingnUp(User user)
        {
            registrationCRUD.RegisterUser(user);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login login)
        {
          
            if (registrationCRUD.VerifyUser(login))
            {
                FormsAuthentication.SetAuthCookie(login.EmailID, true);
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Invalid username or password");
                return View();
            }

        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut(); 
            Session.Abandon(); 
            return RedirectToAction("Login","Registration");
        }
    }
}


