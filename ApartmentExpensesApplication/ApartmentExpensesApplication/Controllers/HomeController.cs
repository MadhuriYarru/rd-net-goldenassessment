﻿using ApartmentExpensesApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ApartmentExpensesApplication.Controllers
{
    [Authorize]
    //[RoutePrefix("Home1")]
    public class HomeController : Controller
    {
        ExpensesCRUD expensesCRUD = new ExpensesCRUD();

       // [Route("Index1")]
        public ActionResult Index()
        { 
            List<ExpenseTracking> expenses = expensesCRUD.GetData();
            return View(expenses);
        }

       // [Route("Create1")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ExpenseTracking expenseTracking)
        {
            expensesCRUD.AddExpenses(expenseTracking);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int ID)
        {
            ExpenseTracking obj = expensesCRUD.GetData().Find(expense => expense.ID == ID);
            return View(obj);
         
        }
        [HttpPost]
        public ActionResult Edit(ExpenseTracking expense)
        {
            expensesCRUD.EditExpenses(expense);
            return RedirectToAction("Index");
        }
      
        public ActionResult Details(int ID)
        {
            ExpenseTracking obj = expensesCRUD.GetData().Find(expense => expense.ID == ID);
            return View(obj);
        }

        public ActionResult Delete(int ID)
        {
            ExpenseTracking obj = expensesCRUD.GetData().Find(expense => expense.ID == ID);
            return View(obj);
        }

        [HttpPost]
        public ActionResult Delete(ExpenseTracking obj)
        {
            expensesCRUD.DeleteData(obj.ID);
            return RedirectToAction("Index");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("About");
        }
    }
}