﻿document.getElementById("getDataButton").addEventListener("click", function () {
	fetch('http://localhost:57857/api/values')
		.then(response => response.text())
		.then(result => {
			document.getElementById("response").innerHTML = result;
		})
		.catch(error => console.error(error));
});
